<?php
/**
 * Created by Dylan Moss.
 * Date: 07/09/2017
 * Description: Create dummy data for Payfast gateway payment management
 */

require_once('autoload.php');
$faker = Faker\Factory::create();

//connect to DB
$conn = mysqli_connect('localhost', 'fastpay-demo','uygnFqxF3vLQCgGe', 'fastpay-demo');

//populate buyers table
for ($iCounter = 0; $iCounter < 10; $iCounter++){
	$sFirstname = $faker->firstName;
	$sSurname = $faker->lastName;
	$sCellNumber = $faker->phoneNumber;
	$sEmail = $faker->safeEmail;
	$sSQL = "INSERT INTO buyer_details (name_first, name_last, email_address, cell_number) VALUE ('$sFirstname', '$sSurname', '$sEmail', '$sCellNumber')";
	mysqli_query($conn, $sSQL);
	echo "$sSQL <br>";
}

//populate transactions table
for ($iCounter = 0; $iCounter < 100; $iCounter++){
	$fAmount = $faker->randomFloat(2, 1, 100000);
	$sItemName = $faker->text($maxNbChars = 100);
	$sItemDescription = $faker->text($maxNbChars = 255);
	$iBuyerID = $faker->numberBetween(0, 10);
	$sSQL = "INSERT INTO transaction_details (amount, item_name, item_description, payment_method, recurring_type, recurring_id, buyer_id) VALUE ('$fAmount', '$sItemName', '$sItemDescription', 'eft', 2, 0, $iBuyerID)";
	mysqli_query($conn, $sSQL);
	echo "$sSQL <br>";
}
