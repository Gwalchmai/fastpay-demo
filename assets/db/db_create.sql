-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2017 at 03:55 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `fastpay-demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `buyer_details`
--

CREATE TABLE `buyer_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_first` varchar(100) NOT NULL,
  `name_last` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `cell_number` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recurring_transaction_details`
--

CREATE TABLE `recurring_transaction_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `original_transaction_id` int(10) UNSIGNED NOT NULL COMMENT 'foreign key: transactions.id',
  `billing_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recurring_amount` decimal(10,2) NOT NULL,
  `frequency` int(11) NOT NULL COMMENT '3- Monthly 4- Quarterly 5- Biannual 6- Annual',
  `cycles` int(11) NOT NULL COMMENT 'The number of payments/cycles that will occur for this subscription. Set to 0 for infinity.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE `transaction_details` (
  `m_payment_id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `payment_method` enum('eft','cc','dc','bc','mp','mc') NOT NULL DEFAULT 'eft' COMMENT ' ‘eft’ – sets eft payment method, ‘cc’ – sets credit card payment method, ‘dc’ – sets debit card payment method, ‘bc’ – sets bitcoin payment method, ‘mp’ – sets masterpass payment method, ‘mc’ – sets mobicred payment method',
  `recurring_type` int(11) NOT NULL COMMENT '1 – sets type to a subscription 2 – sets type to an ad hoc agreement',
  `recurring_id` int(10) UNSIGNED NOT NULL COMMENT 'foreign key: recurring_transaction_details.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buyer_details`
--
ALTER TABLE `buyer_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recurring_transaction_details`
--
ALTER TABLE `recurring_transaction_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD PRIMARY KEY (`m_payment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buyer_details`
--
ALTER TABLE `buyer_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recurring_transaction_details`
--
ALTER TABLE `recurring_transaction_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transaction_details`
--
ALTER TABLE `transaction_details`
  MODIFY `m_payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;
