<?php
/**
 * Created by Dylan Moss.
 * Date: 08/09/2017
 * Description: Transactions pending for a buyer (Payfast gateway payment management)
 */
?>

<table>
	<tr>
		<th>Buyer Name</th>
		<th>Product Name</th>
		<th>Product Description</th>
		<th>Price</th>
		<th></th>
	</tr>
	<?php foreach($aTransactions as $iKey => $aTransaction):
	$sBaseURL = base_url();
	$sSubmitButton = '<input type="button" value="Submit" onclick="this.window.href="' . $sBaseURL . ''; ?>
	?>
		<tr>
			<td><?= $aTransaction['name_first'] ?> <?= $aTransaction['name_last'] ?></td>
			<td><?= $aTransaction['item_name'] ?></td>
			<td><?= $aTransaction['item_description'] ?></td>
			<td><?= $aTransaction['amount'] ?></td>
			<td></td>
		<?php
		</tr>
	<?php endforeach; ?>
</table>

