<?php
/**
 * Created by Dylan Moss.
 * Date: 07/09/2017
 * Description: Controller for Payfast gateway payment management
 */

class payments extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('payments_model');

		//set merchant details
		/*
		 * aMerchantDetails fields definition:
		 *      merchant_id: The Merchant ID, provided by PayFast
		 *      merchant_key: The Merchant Key, provided by PayFast
		 *      return_url: The URL to which PayFast will re-direct after a successful payment
		 *      cancel_url: The URL to which PayFast will re-direct after a failed payment
		 *      notify_url: The URL which is used by PayFast to post the Instant Transaction Notifications (ITNs) for this transaction.
		 */
		$sBaseURL = base_url();
		$this->aMerchantDetails = array('merchant_id' => 'Supplied by FastPay',
		                                'merchant_key' => 'Supplied by FastPay',
		                                'return_url' => "$sBaseURL" . 'payments/success',
		                                'cancel_url' => "$sBaseURL" . 'payments/cancel',
		                                'notify_url' => "$sBaseURL" . 'payments/notify');
		$this->sPassphrase = 'Supplied by FastPay';
		$this->bTestMode = true;
	}

	public function index($iBuyer)
	{
		$aData['aTransactions'] = $this->payments_model->get_pending_transactions($iBuyer);
		$this->load->view('payments/pending', $aData);

	}

	public function submit_payment($iBuyerID, $iTransactionID)
	{
		$sTransactionSignature = '';
		$aBuyer = $this->payment_model->get_buyer_info($iBuyerID);
		$aTransaction = $this->payment_model->get_transaction_info($iTransactionID);

		//put the arrays together for sending to the view
		foreach($this->aMerchantDetails as $sKey => $sVal) {
			if(!empty($sVal)) {
				$sTransactionSignature .= $sKey . ' = ' . urlencode(trim($sVal)) . ' & ';
				$aData['aTransaction']["$sKey"] = $sVal;
			}
		}

		foreach($aBuyer as $sKey => $sVal) {
			if(!empty($sVal)) {
				$sTransactionSignature .= $sKey . ' = ' . urlencode(trim($sVal)) . ' & ';
				$aData['aTransaction']["$sKey"] = $sVal;
			}
		}

		foreach($aTransaction as $sKey => $sValue) {
			if(!empty($sVal)) {
				$sTransactionSignature .= $sKey . ' = ' . urlencode(trim($sVal)) . ' & ';
				$aData['aTransaction']["$sKey"] = $sVal;
			}
		}

		//generate the signature for the transaction and then md5 hash it
		$sTransactionSignature = substr($sTransactionSignature, 0, -1);
		if(isset($passPhrase)) {
			$sTransactionSignature .= ' &passphrase = ' . urlencode(trim($this->sPassphrase));
		}
		$aData['sTransactionHash'] = md5($sTransactionSignature);

	}

}
