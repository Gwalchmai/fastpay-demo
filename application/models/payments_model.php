<?php
/**
 * Created by Dylan Moss.
 * Date: 07/09/2017
 * Description: Model for Payfast gateway payment management
 */

class payments_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_buyer_info($iBuyerID)
	{
		$query = $this->db->get_where('buyer_details', array('id' => $iBuyerID));
		return $query->row_array();
	}

	public function get_transaction_info($iTransactionID)
	{
		$query = $this->db->get_where('transaction_details', array('id' => $iTransactionID));
		return $query->row_array();
	}

	public function get_pending_transactions($iBuyerID)
	{
		$query = $this->db_get_where('fastpay_pending_transaction_view', array('buyer_id' => $iBuyerID));
		return $query->result_array();
	}
}